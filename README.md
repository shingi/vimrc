## About this repository

This repository contains my Vim setup and configuration which I use across dicfferent machines/laptops.

### How to install

- Create a folder to clone to:- eg `~/source`
- Clone this repo under the folder created above `~/source/git clone .....`
- Pull all submodules using `git submodule update --init --recursive`. To update submodules, use `git pull --recurse-submodules`
- Create `~/.vim` folder symbolic link `ln -s ~/source/vimrc ~/.vim`
- Create `~/.vimrc` file synmbolic link `ln -s ~/source/vimrc/vimrc ~/.vimrc`
