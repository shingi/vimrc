set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

" Keep Plugin commands between vundle#begin/end.
" plugin on GitHub repo
" To add a plugin from GitHub, add the plugin as a submodule using the
" following 
" ensure you are in the root folder where this vimrc file resides
" git submodule add <clone URL> bundle/<PLUGIN-NAME>
" git submodule add https://github.com/mechatroner/rainbow_csv.git bundle/rainbow_csv
Plugin 'udalov/kotlin-vim'
Plugin 'mechatroner/rainbow_csv'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line

" filetype on

" enable syntax processing
syntax enable

if has("gui_running")
  set macligatures
  " this one is for linux
  " set guifont=Liberation\ Mono\ for\ Powerline\ 10
  set guifont=Fira_Mono_for_Powerline:h20
  "set guifont=Liberation_Mono_for_Powerline:h18
  set lines=999 columns=999
  "set background=light
  set background=dark
  colorscheme solarized
else
  set background=dark
  colorscheme gruvbox
  set mouse=a
  if &term =~ '256color'
      " disable Background Color Erase (BCE) so that color schemes
      " render properly when inside 256-color tmux and GNU screen.
      " see also http://snk.tuxfamily.org/log/vim-256color-bce.html
      set t_ut=
  endif
endif

" show line numbers
set number

" show last command entered  in the bottom bar
set showcmd

" highlight current line
set cursorline

" this turns on filetype detection and allows loading of language specific indentation files
" based on that detection. For example ~/.vim/indent/python.vim gets loaded when you open a *.py file
filetype indent on

" visual autocomplete for command menu
set wildmenu

" redraw only when we need to
set lazyredraw

" highlight matching [{()}]
set showmatch

" search as characters are entered
set incsearch

" highlight matches
set hlsearch

" enable folding
set foldenable

set nowrap

" number of visual spaces per TAB
set tabstop=4

" number of spaces in tab when editing - the number of spaces that is inserted when you hit <TAB> and 
" also number oof spaces removed when you backspace
set softtabstop=4

set shiftwidth=4

" tabs are spaces
set expandtab

set smartindent
set autoindent

" Turn off beep
set visualbell

let g:airline#extensions#tabline#enabled = 1

let g:airline_powerline_fonts = 1

